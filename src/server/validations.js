import validator from "validator";

export const textFieldValidator = (min = 1, max = 15, value = "", req = true) => {
    let error = "";
    if (!value && req) {
        error = "This field is required!";
    } else if (value) {
        if (value.length < min) {
            error = `Min. length is ${min} characters!`;
        } else if (value.length > max) {
            error = `Max. length is ${max} characters!`;
        } else {
            if (!validator.isAlpha(value)) error = "Please enter only alphabets";
        }
    }
    return error;
};



export const emailFieldValidator = (value = "", allContacts, id, req = true) => {
    let error = "";
    if (!value && req) {
        error = "This field is required!";
    } else if (value) {
        if (!validator.isEmail(value)) error = "Please enter a valid email!";
        if (allContacts && allContacts.length > 0) {
            const item = allContacts.find(function (el) {
                return el.isActive && el.email === value.toLowerCase();
            });
            if (item && item.id !== id) error = "This email is already in use!";
        }
    }
    return error;
};
