import React, { memo } from "react";
import "./index.css";
import PropTypes from "prop-types";
import { Button } from "reactstrap";

const ContactList = (props) => {
    const { contacts, searchText, onClickContact, showContact } = props;
    const newData = {};

    const contactValues = (prevData) => {
        if (prevData && prevData.length > 0) {
            let contact = prevData.filter((el) => el.isActive === true).sort(function compare(a, b) {
                if (a.firstName > b.firstName) {
                    return 1;
                }
                return 0;
            });

            if (searchText) {
                contact = contact.filter((el) =>
                    `${el.firstName} ${el.lastName}`
                        .toLowerCase()
                        .includes(searchText.toLowerCase())
                );
            }

            contact.forEach((el) => {
                const data = el.firstName.toLowerCase().charAt(0);
                if (newData[data]) {
                    newData[data] = [...newData[data], el.id];
                } else {
                    newData[data] = [el.id];
                }
            });

            const contactData = Object.keys(newData).map((el) => {
                return {
                    data: contact.filter((key) =>
                        newData[el.toLowerCase()].includes(key.id)
                    ),
                };
            });
            return contactData;
        }
    };
    return (
        <div className="list-menu">
            {contactValues(contacts).map((el, index) => {
                return (
                    <div className="text-a" key={`out-${index}`}>
                        <ul>
                            {el.data.map((item) => (
                                <li key={`in-${item.id}`}>
                                    <Button
                                        color={
                                            showContact?.id === item.id
                                                ? "primary btn-block mb-2"
                                                : "link"
                                        }
                                        onClick={() => {
                                            onClickContact(item.id);
                                        }}
                                    >
                                        {item.firstName}{" "}
                                        {item?.middleName ? `${item?.middleName.charAt(0)}.` : ""}{" "}
                                        {item.lastName}
                                    </Button>
                                </li>
                            ))}
                        </ul>
                    </div>
                );
            })}
        </div>
    );
};

ContactList.propTypes = {
    contacts: PropTypes.array,
    searchText: PropTypes.string,
    onClickContact: PropTypes.func,
    showContact: PropTypes.object,
};
export default memo(ContactList);
