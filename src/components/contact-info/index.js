import { Button } from "reactstrap";
import "./index.css";
import PropTypes from "prop-types";
import userImage from "../../assets/images/user.jpg";

const ContactInfo = (props) => {
    const { showContact, DeleteContact, onClickEditContact } = props;
    return (
        <div className="right-sec">
            {showContact ? (
                <>
                    <div className="profile-detail ">
                        <div className="pro-img">
                            <img src={userImage} className="dp-img" alt=""></img>
                        </div>
                        <div className="p-name">
                            <h3>
                                {showContact.firstName} {showContact?.middleName}{" "}
                                {showContact.lastName}
                            </h3>
                            <div className="mail-cont">
                                <div className="d-flex mt-5">
                                    <div className="mr-4">
                                        <b>Email -</b>{" "}
                                    </div>
                                    {showContact.email}
                                </div>
                                <div className="d-flex mt-3">
                                    <div className="mr-4">
                                        <b>Group -</b>{" "}
                                    </div>
                                    {showContact.group}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="btn-footer d-flex pl-3">
                        <div className="btn-left d-flex mx-n2">
                            <Button className="mx-2" color="outline-primary">
                                Share
              </Button>
                            <Button
                                className="mx-2"
                                color="outline-primary"
                                onClick={onClickEditContact}
                            >
                                Edit
              </Button>
                        </div>
                        <div className="btn-left ml-auto">
                            <Button
                                color="danger"
                                className="mr-3"
                                block
                                onClick={DeleteContact}
                            >
                                Delete
              </Button>
                        </div>
                    </div>
                </>
            ) : (
                    "No Contact"
                )}
        </div>
    );
};

ContactInfo.propTypes = {
    showContact: PropTypes.object,
    DeleteContact: PropTypes.func,
    onClickEditContact: PropTypes.func,
};
export default ContactInfo;
