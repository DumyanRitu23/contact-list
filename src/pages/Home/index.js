import React, { Component } from "react";
import { Button } from "reactstrap";
import "./index.css";
import { contactsData } from "../../server/contacts-data";
import {
    ConfirmationModal,
    ContactDetail,
    AddContact,
    ContactList,
} from "../../components";

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: contactsData,
            searchText: "",
            showContact: null,
            formData: {
                type: "Add",
                data: null,
            },
            toggleContactForm: false,
            toggleConfirmation: false,
        };
    }
    componentDidMount() {
        this.setState({
            showContact: this.searchContact(),
        });
    }
    searchContact = () => {
        const { list } = this.state;
        const item = list
            .find(function (el) {
                return el.isActive === true;
            });
        return item ?? null;
    };
    handleSearchChange = (e) => {
        this.setState({
            searchText: e.target.value,
        });
    };
    onClickContact = (id) => {
        const { list } = this.state;
        const item = list.find(function (el) {
            return el.id === id;
        });
        if (item) {
            this.setState({
                showContact: item,
            });
        }
    };
    DeleteContact = () => {
        this.toggleConfirmationModal();
    };
    DeleteContactConfirm = (id) => {
        const { list } = this.state;
        const newList = list.map((key) => {
            if (key.id === id) {
                key.isActive = false;
            }
            return key;
        });
        this.setState(
            {
                list: newList,
            },
            () => {
                this.DeleteContact();

            }
        );
    };
    onClickAddContact = () => {
        const { toggleContactForm } = this.state;
        this.setState({
            formData: {
                type: "Add",
                data: null,
            },
            toggleContactForm: !toggleContactForm,
        });
    };
    onClickEditContact = () => {
        const { showContact, toggleContactForm } = this.state;
        this.setState({
            formData: {
                type: "Edit",
                data: showContact,
            },
            toggleContactForm: !toggleContactForm,
        });
    };
    addContact = (addData) => {
        const { list } = this.state;
        if (addData.errors) delete addData.errors;
        addData.isActive = true;
        this.setState(
            {
                list: [...list, addData],
            },
        );
    };
    editContact = (addData) => {
        const { list } = this.state;
        if (addData.errors) delete addData.errors;

        const newList = list.map((el) => {
            if (el.id === addData.id) {
                return addData;
            } else {
                return el;
            }
        });
        this.setState({
            list: newList,
            showContact: addData,
        });
    };
    toggleContactModal = () => {
        const { toggleContactForm } = this.state;
        this.setState({
            toggleContactForm: !toggleContactForm,
        });
    };
    toggleConfirmationModal = () => {
        const { toggleConfirmation } = this.state;
        this.setState({
            toggleConfirmation: !toggleConfirmation,
        });
    };
    render() {
        const {
            list,
            searchText,
            showContact,
            formData,
            toggleContactForm,
            toggleConfirmation,
        } = this.state;
        return (
            <div className="main-section">
                <div className="main-body">
                    <div className="list">
                        <div className="form-group has-search">
                            <span className="fa fa-search form-control-feedback"></span>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Search"
                                value={searchText}
                                onChange={this.handleSearchChange}
                            />
                        </div>
                        <ContactList
                            contacts={list}
                            searchText={searchText}
                            onClickContact={this.onClickContact}
                            showContact={showContact}
                        />
                        <div className="add-btn">
                            <Button
                                color="danger "
                                className="btn-block mt-3"
                                onClick={this.onClickAddContact}
                            >
                                ADD
              </Button>
                        </div>
                    </div>
                    <ContactDetail
                        showContact={showContact}
                        DeleteContact={this.DeleteContact}
                        onClickEditContact={this.onClickEditContact}
                    />
                </div>
                <AddContact
                    formData={formData}
                    addContact={this.addContact}
                    editContact={this.editContact}
                    isOpen={toggleContactForm}
                    toggleContactModal={this.toggleContactModal}
                    allContacts={list}
                />
                <ConfirmationModal
                    isOpen={toggleConfirmation}
                    toggleModal={this.toggleConfirmationModal}
                    onConfirm={() => this.DeleteContactConfirm(showContact.id)}
                    text="This record will be deleted."
                />
            </div>
        );
    }
}

export default Home;
